using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Defining the movement states used for controlling the supports
// Fixe does nothing, Positif extends the support, Negatif retracts the support
public enum EtatSupport { Fixe = 0, Positif = 1, Negatif = -1 };

public class SupportController : MonoBehaviour
{
    // Initial state of the support is Fixe, meaning it's at rest
    public EtatSupport supportState = EtatSupport.Fixe;
    // Default movement speed, public means it can be modified in the Inspector
    public float speed = 30.0f;

    private ArticulationBody articulation;

    void Start()
    {
        articulation = GetComponent<ArticulationBody>();
    }

    void FixedUpdate() // FixedUpdate is like Update but synchronized with Unity's physics engine
    {
        if (supportState != EtatSupport.Fixe)
        {
            // Calculate the change in movement based on the state, speed, and time
            float movementChange = (float)supportState * speed * Time.fixedDeltaTime;
            float targetPosition = CurrentPrimaryAxisPosition() + movementChange;
            MoveTo(targetPosition);
        }
    }

    // Get the current primary axis position (used for calculating the target position)
    float CurrentPrimaryAxisPosition()
    {
        float currentAxisRads = articulation.jointPosition[0];
        float currentPosition = Mathf.Rad2Deg * currentAxisRads; // Convert from radians to degrees
        return currentPosition;
    }

    // Move the support to the target position
    void MoveTo(float primaryAxisPosition)
    {
        var drive = articulation.xDrive;
        drive.target = primaryAxisPosition;
        articulation.xDrive = drive;
    }
}
