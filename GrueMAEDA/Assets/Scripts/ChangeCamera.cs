using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ChangeCamera : MonoBehaviour
{
    public Camera mainCamera;
    public Camera povCamera;

    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        // Switch camera perspective
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (mainCamera.enabled)
            {
                mainCamera.enabled = false;
                povCamera.enabled = true;
            }
            else
            {
                mainCamera.enabled = true;
               povCamera.enabled = false;
            }
        }
    }
}